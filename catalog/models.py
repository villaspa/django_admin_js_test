from django.db import models
from froala_editor.fields import FroalaField


class Category(models.Model):
    name = models.CharField(
        max_length=150, blank=False,
        unique=True,
        verbose_name='Name',
        help_text='Name of the category')
    comments = models.TextField(
        max_length=500, blank=True,
        verbose_name='Comments',
        help_text='Comments')

    class Meta:
        managed = True
        ordering = ['name', ]
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Product(models.Model):
    sku = models.PositiveIntegerField(
        blank=False, unique=True,
        verbose_name='SKU',
        help_text='Unique number used as SKU / part number')
    name = models.CharField(
        max_length=150, blank=False, unique=True,
        verbose_name='Name',
        help_text='Name of the product')
    description = FroalaField(
        max_length=1000, blank=False,
        verbose_name='Description',
        help_text='Product description')
    categories = models.ManyToManyField(
        Category, related_name='products',
        blank=True,
        verbose_name='Categories',
        help_text='Product to Category relation')

    class Meta:
        managed = True
        ordering = ['name', ]
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return str(self.name) + ' [' + str(self.sku) + ']'
