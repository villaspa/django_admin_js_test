from django.contrib import admin
from .models import *


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    fields = ('name', 'comments', )


class ProductAdmin(admin.ModelAdmin):
    list_display = ('sku', 'name', )
    fields = ('sku', 'name', 'description', 'categories', )
    filter_horizontal = ['categories', ]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
